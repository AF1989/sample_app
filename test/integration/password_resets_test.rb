require "test_helper"

class PasswordResetsTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  def setup
    ActionMailer::Base.deliveries.clear
    @user = users(:michael)
  end

  test "password reset" do
    get new_password_reset_path
    assert_template 'password_resets/new'
    # Невірний адрес електронної пошти
    post password_resets_path, params: {password_reset: {email: ""}}
    assert_not flash.empty?
    assert_template 'password_resets/new'
    # Вірний адрес електроної пошти
    post password_resets_path, params: {password_reset: {email: @user.email}}
    assert_not_equal @user.reset_digest, @user.reload.reset_digest
    assert_equal 1, ActionMailer::Base.deliveries.size
    assert_not flash.empty?
    assert_redirected_to root_url
    #форма скидування пароля
    user = assigns(:user)
    #Невірний адрес електроної пошти
    get edit_password_reset_path(user.reset_token, email: "")
    assert_redirected_to root_url
    #Неактивована учетна запись
    user.toggle!(:activated)
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_redirected_to root_url
    user.toggle!(:activated)
    #Вірний адрес електронної пошти, невірний токен
    get edit_password_reset_path('wrong token', email: user.email)
    assert_redirected_to root_url
    #Віринй адрес електронної пошти, вірний токен
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_template 'password_reset/edit'
    assert_select "input[name=email][type=hidden][value=?]", user.email
    #Недопустимий пароль і підтвердження
    patch password_reset_path(user.reset_token),
          email: user.email,
          user: { password: "foobaz", password_confirmation: "barquux" }
    assert_select 'div#error_explanation'
    #Пустий пароль
    patch password_reset_path(user.reset_token),
          email: user.email,
          user: { password: " ", password_confirmation: "foobar" }
    assert_not flash.empty?
    assert_template 'password_resets/edit'
    # Допустимий пароль і підтвердження
    patch password_reset_path(user.reset_token),
          email: user.email,
          user: { password: "foobaz", password_confirmation: "foobaz" }
    assert is_logged_in?
    assert_not flash.empty?
    assert_redirected_to user
  end
end
