class User < ApplicationRecord
  attr_accessor :remember_token, :activation_token, :reset_token
  before_save :downcase_email
  before_create :create_activation_digest
  validates :name, presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true,  length: { maximum: 255 }
  validates :email, format: {with: VALID_EMAIL_REGEX},
            uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, length: {minimum: 6}, allow_blank: true

  def User.digest(string)
    cost=ActiveModel::SecurePassword.min_cost ?
           BCrypt::Engine::MIN_COST :
           BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # повертає випадковий токен
  def User.new_token
    SecureRandom.urlsafe_base64
  end

  #запамятати користувача в базі даних для використання в постійних сесіях
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  #Повертає true, якщо вказаний токен співпадає дайджесту
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  # Забуває юзера
  def forget
    update_attribute(:remember_digest, nil)
  end

  #Активовує учьотну запись
  def activate
    update_attribute(:activated, true)
    update_attribute(:activated_at, Time.zone.now)
  end

  #Посилає листа із лінкою на сторінку активації
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

  #Встановлює атрибути для скидання пароля
  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest, User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  # Посилає ласта із силкою на форму ввода нового пароля
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  #Повертає тру, якщо час для скидання паролю вийшов
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end

  private

  # Перетворює адрес електронної пошти в нижній регістер
  def downcase_email
    self.email = email.downcase
  end

  #Створює і присвоює токен активації і його дайджест
  def create_activation_digest
    self.activation_token = User.new_token
    self.activation_digest = User.digest(activation_token)
  end
end
