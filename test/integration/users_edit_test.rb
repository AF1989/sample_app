require "test_helper"

class UsersEditTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  def log_in_as(user, password: 'password')
    post login_path, params: { session: { email: user.email, password: password} }
  end

  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    user_params = {
      name: "",
      email: "user@invalid",
      password: "foo",
      password_confirmation: "bar",
    }
    patch user_path, params: { user: user_params }
    assert_template 'users/edit'
  end

  test "successful edit with" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    user_params = {
      name: "Foo Bar",
      email: "goo@bar.com",
      password: "",
      password_confirmation: "",
    }
    patch user_path, params: { user: user_params }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload

    assert_equal @user.name, user_params[:name]
    assert_equal @user.email, user_params[:email]
  end
end
