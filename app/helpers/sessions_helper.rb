module SessionsHelper
  def log_in(user)
    session[:user_id] = user.id
  end

  #Запамятовує юзера в постійному сеансі
  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end

  # Повертає true, якщо даний користувач є текущим
  def current_user?(user)
    user == current_user
  end

  # Повертає юзера, який відповідає токену в cookie
  def current_user
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
      if user && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end

  # повертає true, якщо юзер зареєструвався, або false
  def logged_in?
    !current_user.nil?
  end

  # закриває постійний сеанс
  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

  #здійнює вихід поточного користувача
  def log_out
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end

  # Перенаправляє по збереженому адресу або на сторінку по замовчуванню
  def redirect_back_or(default)
    redirect_to (session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # Запамятовуємо урлу
  def store_location
    session[:forwarding_url] = request.url if request.get?
  end
end
